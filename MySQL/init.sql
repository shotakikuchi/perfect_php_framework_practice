mysql -u root -p
# 初期passwordは root

CREATE database mini_blog DEFAULT CHARACTER SET utf8;

create user 'dbuser'@'localhost' identified by '21911';
grant all privileges ON mini_blog.* TO 'dbuser'@'localhost';

use mini_blog;

CREATE TABLE user(
 id INTEGER  auto_increment,
 user_name VARCHAR(20) NOT NULL,
 password VARCHAR(40) NOT NULL,
 created_at DATETIME,
 PRIMARY KEY(id),
 UNIQUE KEY user_name_index(user_name)
)ENGINE = INNODB;

CREATE TABLE following(
user_id INTEGER,
following_id INTEGER,
PRIMARY KEY (user_id, following_id)
)ENGINE = INNODB;

CREATE TABLE status(
id INTEGER auto_increment,
user_id INTEGER NOT NULL,
body VARCHAR(255),
 created_at DATETIME,
 PRIMARY KEY(id),
  INDEX user_id_index(user_id)
)ENGINE = INNODB;

ALTER TABLE following ADD FOREIGN KEY (user_id) REFERENCES user(id);
ALTER table following ADD FOREIGN KEY (following_id) REFERENCES user(id);
ALTER TABLE status ADD FOREIGN KEY (user_id) REFERENCES user(id);