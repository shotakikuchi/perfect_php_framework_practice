<?php

/**
 * Created by PhpStorm.
 * User: kikuchishota
 * Date: 2016/04/30
 * Time: 23:01
 */
class ClassLoader
{
    protected $dirs;

    public function register()
    {
        spl_autoload_register(array($this, 'loadClass'));
    }

    public function registerDir($dir)
    {
        $this->dirs[] = $dir;
    }

    public function loadClass($class)
    {
        foreach ($this->dirs as $dir) {
            $file = $dir . '/' . $class . '.php';
            if (is_readable($file)) {
                require $file;

                return;
            }
        }
    }

    public function getDir(){
        var_dump($this->dirs);
    }
}