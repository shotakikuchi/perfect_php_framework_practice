<?php

class Request
{
    public function isPost()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            return true;
        }

        return false;
    }

    public function getGet($name, $default = null)
    {
        if (isset($_GET[$name])) {
            return $_GET[$name];
        }
        return $default;

    }

    public function getPost($name, $default = null)
    {
        if (isset($_POST[$name])) {
            return $_POST[$name];
        }
        return $default;
    }

    public function getHost()
    {
        if (!empty($_SERVER['HTTP_HOST'])) {
            return $_SERVER['HTTP_HOST'];
        }
        return $_SERVER['SERVER_NAME'];
    }

    public function isSsl()
    {
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on') {
            return true;
        }
        return false;
    }

    public function getRequestUri()
    {
        return $_SERVER['REQUEST_URI'];
    }

    //foo/bar/にフロントコントローラー(index.php)があるとする。
    //ホスト名 : http://example.com
    //BaseUrlはフロントコントローラーまでのパスを取得する。
    //http://example.com/foo/bar/index.php/list -> BaseUrl : foo/bar/
    //http://example.com/foo/bar/list?foo=bar -> BaseUrl : foo/bar/
    public function getBaseUrl()
    {
        $script_name = $_SERVER['SCRIPT_NAME'];
        $request_uri = $this->getRequestUri();

        //http://example.com/foo/bar/index.php でアクセスが来た場合(RequestURI)
        //http://example.com/foo/bar/index.php -> BaseUrl === ScriptUrl なので /foo/bar/index.php をそのまま返す。
        if (0 === strpos($request_uri, $script_name)) {
            return $script_name;

            //http://example.com/foo/bar/ -> BaseUrl : /foo/bar/
            //dirnameはファイルパスからディレクトリパスのみを抜き出すメソッド
        } else if (0 === strpos($request_uri, dirname($script_name))) {
            // 最後の / を取り除く
            return rtrim(dirname($script_name), '/');
        }

        return '';
    }

    //PathInfoはフロントコントローラーより後のパスを取得する。
    //www/foo/bar/にフロントコントローラーがあるとする
    //http://example.com/foo/bar/index.php/list -> pathInfo : /list
    //http://example.com/foo/bar/list -> pathInfo : /list (index.phpが省略されている)
    public function getPathInfo()
    {
        //BaseUrl : /foo/bar
        $base_url = $this->getBaseUrl();

        //RequestUrl pattern1 : /foo/bar/list?foo=bar
        //RequestUrl pattern2 : /foo/bar/index.php/list
        //RequestUrl pattern3 : /foo/bar/list (index.phpを省略)
        $request_uri = $this->getRequestUri();

        //pattern1 : /foo/bar/list?foo=bar => RequestUrl を /foo/bar/list に変換する。
        if (false !== ($pos = strpos($request_uri, '?'))) {
            $request_uri = substr($request_uri, 0, $pos);
        }

        // RequestUrl : /foo/bar/index.php/list => PathInfo : list
        // RequestUrl : /foo/bar/list => PathInfo : list
        $path_info = (string)substr($request_uri, strlen($base_url));

        $path_info = rtrim($path_info,'/');

        return $path_info;
    }
}