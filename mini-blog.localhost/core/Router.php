<?php

class Router
{
    protected $routes;

    public function __construct($definitions)
    {
        $this->routes = $this->compileRouters($definitions);
    }

    public function compileRouters($definitions)
    {
        $routes = array();

        //$definitions
        //array(
        //  '/item/:action'
        //  => array('controller' => 'item' , 'action' => 動的にする),
        //);
        foreach ($definitions as $url => $params) {
            //この関数は文字列の最初から空白文字を取り除き、 取り除かれた文字列を返します。

            // /item/:action => item/:action => item , :action
            // ltrimは最後の / を取り除く
            $tokens = explode('/', ltrim($url, '/'));
            foreach ($tokens as $i => $token) {
                // :action の場合
                if (0 === strpos($token, ':')) {
                    // $name = action
                    $name = substr($token, 1);
                    //$token = '(?P<action>[^/]+)';
                    $token = '(?P<' . $name . '>[^/]+)';
                }
                $tokens[$i] = $token;
            }
            // /item/(?P<action>[^/]+)
            $pattern = '/' . implode('/', $tokens);
            //$routes['/item/(?P<action>[^/]+)'] = array('controller' => 'item' , 'action' => 動的)
            $routes[$pattern] = $params;
        }
        return $routes;
    }

    public function resolve($path_info)
    {
        if ('/' !== substr($path_info, 0, 1)) {
            $path_info = '/' . $path_info;
        }

        foreach ($this->routes as $pattern => $params) {

            // user/:action の中の :actionに相当する部分を動的に取得し array('action' => 'sighup') のように取得する。
            //それを元のルーティングとarray_mergeするため
//            '/account/:action'
//            => array('controller' => 'account'),
//            が
//            '/account/signup'
//            => array('controller' => 'account', 'action => 'signup'),
//              のように動的に書きかわる。

            if (preg_match('#^' . $pattern . '$#', $path_info, $matches)) {
                $params = array_merge($params, $matches);

                return $params;
            }
        }

        return false;
    }
}