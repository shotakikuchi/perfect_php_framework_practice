<?php

/**
 * Created by PhpStorm.
 * User: kikuchishota
 * Date: 2016/05/05
 * Time: 3:07
 */
class MiniBlogApplication extends Application
{
    //accountコントローラーの signin Actionを呼び出す
    protected $login_action = array('account', 'signin');

    public function getRootDir()
    {
        // TODO: Implement getRootDir() method.
        return dirname(__FILE__);
    }

    protected function registerRoutes()
    {
        // TODO: Implement registerRoutes() method.
        return array(
            '/'
            => array('controller' => 'status','action' => 'index'),
            '/status/post'
            => array('controller' => 'status','action' => 'post'),

            '/account'
            => array('controller' => 'account', 'action' => 'index'),
            '/account/:action'
            => array('controller' => 'account'),

            //StatusControllerの他のルーティング
            '/user/:user_name'
            => array('controller' => 'status', 'action' => 'user'),
            '/user/:user_name/status/:id'
            => array('controller' => 'status', 'action' => 'show'),

            //followのルーティング
            '/follow'
            => array('controller' => 'account', 'action' => 'follow'),
        );
    }

    protected function configure()
    {
        $this->db_manager->connect('master', array(
            'dsn' => 'mysql:dbname=mini_blog;host=localhost',
            'user' => 'dbuser',
            'password' => '21911',
        ));
    }
}